# Specification of the synchronisation between curator and bmarchives.org

This repository defines the process to push data from the internal curator database to bmarchvies.org. Docuteam pushes CSV files to a ftp server hosted by CS2.

See [Legacy OAI-PMH API](legacy_oai_pmh_api.md) for a description of the old API and how data available in the CSV files is related to it.

## Sync interval

## Data format

Files:

- archival_units.csv
- archiv_catalog.csv
- geography.csv
- image_attributes.csv
- individuals.csv
- institutions.csv
- map_attributes.csv
- photographers_photo_studios.csv
- series_types.csv
- tags.csv
- taxonomies.csv
- themes.csv

Where not specified otherwise, the data in the CSV columns should be treated as text.

### archival_units.csv

**Note: Contains only the leaf archival units (=the actual documents)**

- `id`: unique numerical id for each archival unit
- `parent_id`: id of the archive catalog entry in the file `archive_catalog.csv` to which this archival unit belongs
- `full_order_nr`: unique numerical full order number; defines the ordering of the full archival unit tree; the full order number of a child node starts with the full order number of their respective parent
- `type`: type of the archival unit; either `Image`, `Map` or `Text`
- `full_reference_code`: full reference code of the archival unit
- `old_reference_code`
- `title`
- `date_from`: date in format `yyyymmdd`
- `date_to`: date in format `yyyymmdd`
- `date_text`: additional notes about date
- `date_string`: readable string containing the values from `date_from`, `date_to` and `date_text`
- `extent`
- `archival_history`
- `related_uod`
- `scope`
- `notes`
- `image_iconography`
- `image_copyright`
- `image_stamps`
- `image_width`
- `image_height`
- `image_dept`
- `image_width_support`
- `image_height_support`
- `image_attribute_ids`: ids of image attributes assigned to the archival unit; values are separated by `|`
- `map_iconography`
- `map_verso`
- `map_scale`
- `map_width`
- `map_height`
- `map_attribute_ids`: ids of map attributes assigned to the archival unit; values are separated by `|`
- `creator_ids`: ids of taxonomies with relation type `Creator`; values are separated by `|`
- `individual_ids`: ids of **all** taxonomies of type `Person` which are related to this archival unit; values are separated by `|`; also refers to `id` in file `individuals.csv`
- `photographer_ids`: ids of **all** taxonomies of type `Person` or `Corporate Identity` which belong to the category `Photographers / Photo Studios` and are related to this archival unit; values are separated by `|`; also refers to `id` in file `photographers_photo_studios.csv`
- `institution_ids`: ids of **all** taxonomies of type `Corporate Identity` which are related to this archival unit; values are separated by `|`; also refers to `id` in file `institutions.csv`
- `institution_ids`: ids of **all** taxonomies of type `Geography` which are related to this archival unit; values are separated by `|`; also refers to `id` in file `geography.csv`
- `theme_ids`: ids of **all** taxonomies of type `Key Word` which are related to this archival unit; values are separated by `|`; also refers to `id` in file `themes.csv`
- `series_type_ids`: ids of **all** `Series types` (file `series_types.csv`) which are related to this archival unit; values are separated by `|`
- `tag_ids`: ids of tags assigned to the archival unit; values are separated by `|`

### archive_catalog.csv

- `id`: unique numerical id for each archive catalog entry
- `parent_id`: id of the parent archive catalog entry
- `label`: label to display in the archive catalog list

### geography.csv

- `id`: unique numerical id for each geography entry; same `id` as in the file `taxonomies.csv`
- `parent_ids`: ids of the parent geography entries; values are separated by `|`
- `label`: label to display in the geography list

### image_attributes.csv

- `id`: unique numerical id for each image attribute
- `type`: type of the image attribute; should be treated as a label, **values might change**
- `value`: unique numerical id for each image attribute

### individuals.csv

- `id`: unique numerical id for each individual; same `id` as in the file `taxonomies.csv`
- `label`: label to display in the 'Individuals' list

### institutions.csv

- `id`: unique numerical id for each institution; same `id` as in the file `taxonomies.csv`
- `label`: label to display in the 'Insititution' list

### map_attributes.csv

- `id`: unique numerical id for each map attribute
- `type`: type of the image attribute; should be treated as a label, **values might change**
- `value`: unique numerical id for each image attribute

### photographers_photo_studios.csv

- `id`: unique numerical id for each photographer / photo studio; same `id` as in the file `taxonomies.csv`
- `label`: label to display in the 'Photographers / Photo Studios' list

### series_types.csv

- `id`: unique numerical id for each series type
- `label`: label to display in the 'Series type' list

### tags.csv

- `id`: unique numerical id for each tag
- `name`: name of the tag

### taxonomies.csv

- `id`: unique numerical id for each taxonomy
- `type`: type of the taxonomy; **values might change**
- `name`
- `parallel_name`
- `other_name`
- `identifier`
- `date_start`: date in format `yyyymmdd`
- `date_end`: date in format `yyyymmdd`
- `history`
- `places`
- `legal`
- `functions`
- `mandates`
- `structure`
- `context`
- `event`
- `date`
- `language`
- `sources`
- `relation_type_and_taxonomy_ids`: relation type and ids of taxonomies related to this taxonomy; values are separated by `|`, each value consists of the relation type and a taxonomy id separated by `:` (`<relation type>:<taxnomy id>`); **the labels of relation types will change** 

### themes.csv

- `id`: unique numerical id for each theme
- `parent_ids`: ids of the parent themes; values are separated by `|`
- `label`: label to display in the 'Themes' list
