# Legacy OAI-PMH API

The old API exposed data as Dublin Core XML via an OAI-PMH service. This document describes which columns in the CSV files correspond to which XML tags. The headers of the following sections are named after the respective XML tag.

## dc:title

archival_units.csv, column `title`, substring after string `[English:`, if present or full value otherwise

## dc:title_2

archival_units.csv, column `title`, substrings enclosed in and starting with strings `[English: ` or `[German: ` till next occurence of `]`, if present or empty string otherwise

## dc:title_3

archival_units.csv, column `title`

## dc:identifier

archival_units.csv, columns `full_reference_code`

## dc:date

archival_units.csv, columns `date_from` (format `yyyy-mm-dd`), `date_to` (format `yyyy-mm-dd`); separated by `/`

## dc:date_2

archival_units.csv, string `Date early: ` + column `date_from` (format `dd.mm.yyyy`)

## dc:date_3

archival_units.csv, string `Date late: ` + column `date_to` (format `dd.mm.yyyy`)

## dc:date_4

archival_units.csv, column `date_string`

## dc:date_5

archival_units.csv, column `archival_history` substring from `Acquisition year: `

## dc:description

archival_units.csv, columns `31_scope`, `image_iconography`, `image_stamps`, `comment` and `map_verso`

## dc:subject

all taxonomies of types `Person`, `Coporate Identity`, `Key word` and `Geography` related to a archival unit; plus all taxonomies related to these taxonomies with relation `Photographers / Photo Studios`; plus the the fields `full_reference_code` and `title` of every parent archival unit from root to corresponding archival unit

## dc:creator

all taxonomies of with a relation of type `Creator`, `Fotograf`, `Provenienz`, `Autor` and `Fotostudio`
## dc:type

archival_units.csv, column `type`

## dc:format

For archival untis of type `Image`:

archival_units.csv, columns `image_height`, `image_width`, `image_depth`,`image_height_support`, `image_width_support`; image attributes types `Process`, `Type of support`, `Special format`, `Type of images`, `Type of slides`

For archival units of type `Map`:

archival_units.csv, columns `map_height`, `map_width`, `map_scale`; map attributes types `Type of plan`, `Detail`, `Material`

For archival units of type `Text`:

archival_units.csv, column `extent`

## dc:relation

archival_units.csv, column `related_uod`, substring after `Same images: ` 
